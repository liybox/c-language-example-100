# Git 命令积累

mkdir xxx 创建文件夹
cd/pwd

git init 仓库初始化
ls -sh 查看隐藏文件
git commit -m "备注信息"
git add xxx
git status  当前仓库状态
git diff 查看修改的代码量

git log 输出日志

git reset --hard HEAD^   退回上一个版本  ^^上上个版本

``` 
git reflog  查看自己输入过的命令
git reset --hard xxxx  退回到某个版本
cat xxx   查看文件
git diff HEAD -- readme.txt   比较两个版本的文件
```